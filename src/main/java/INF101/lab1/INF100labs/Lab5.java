

package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.List;

/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/5/
 */
public class Lab5 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        ArrayList<Integer> a = new ArrayList<>();
        a.add(3);
        a.add(5);
        a.add(3);
        a.add(7);
        a.add(8);
        a.add(3);
        System.out.print(a);
        //removeThrees(a);
        System.out.println();
        uniqueValues(a);

        ArrayList<Integer> t = new ArrayList<>();
        ArrayList<Integer> s = new ArrayList<>();
        t.add(1);
        t.add(3);
        t.add(5);
        s.add(2);
        s.add(7);
        s.add(9);
        addList(t, s);


    }

    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {

        while (list.contains(3)) {
            list.remove((Integer)3);
        //System.out.print(list); for eget syns skyld
        }
        return list;
        
        
    }
        

    

    public static List<Integer> uniqueValues(ArrayList<Integer> list) {
        List<Integer>liste2 = new ArrayList<Integer>();
        for(int i=0;i<list.size();i++)
        {
            //System.out.println(list.get(i)); for egen skyld. Hjelper med overblikket
            int x;
            if(!liste2.contains(list.get(i)))
            {
                x=list.get(i);
                liste2.add(x);
            }
        }
        System.out.println(liste2);
        return liste2;
        
    }

    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {
        for(int i=0;i<a.size();i++) {
            a.set(i, (a.get(i)+b.get(i)));
        }
        System.out.println(a);
        System.out.println(b);
    }

}
