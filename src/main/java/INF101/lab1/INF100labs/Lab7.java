package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022. You can find
 * them here: https://inf100.ii.uib.no/lab/7/
 */

 public class Lab7{

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        ArrayList<ArrayList<Integer>> grid1 = new ArrayList<>();
        grid1.add(new ArrayList<>(Arrays.asList(3, 0, 9)));
        grid1.add(new ArrayList<>(Arrays.asList(4, 5, 3)));
        grid1.add(new ArrayList<>(Arrays.asList(6, 8, 1)));
        System.out.println(allRowsAndColsAreEqualSum(grid1));

}

    

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
         grid.remove(row);
        System.out.println(grid);
        
    }

    
    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
            
        int radSum = 0, kolonneSum = 0; //sette en variabel som skal fungere som en løpende teller her
       
        for (int i = 0; i < grid.size(); i++) { //definere hva den første indeksen i både kolonnen og raden har tilsammen.
            radSum += grid.get(0).get(i);
            kolonneSum += grid.get(i).get(0);
        }
        // Gå løpende gjennom kolonnene og sjekke at de har samme sum som første. Samme med radene.
        for (int i = 1; i < grid.size(); i++) {
            int oppdatertRadSum = 0, oppdatertKolonneSum = 0;
            for (int j = 0; j < grid.get(i).size(); j++) {
                oppdatertRadSum+= grid.get(i).get(j);
                oppdatertKolonneSum+= grid.get(j).get(i);
            }
            if ( oppdatertRadSum!= radSum || oppdatertKolonneSum != kolonneSum) {
                return false;
            }
        }
        return true;
       
    }

}
    


