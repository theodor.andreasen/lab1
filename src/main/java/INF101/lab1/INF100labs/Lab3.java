package INF101.lab1.INF100labs;

/**
 * Implement the methods multiplesOfSevenUpTo, multiplicationTable and crossSum.
 * These programming tasks was part of lab3 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/3/
 */
public class Lab3 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        multiplesOfSevenUpTo(49);
        multiplicationTable(5);
        crossSum(123);
    

    }

    public static void multiplesOfSevenUpTo(int n) {
        int t=7;
        while (n-7>=0 && n>=t) {
            System.out.println(t);
            t+=7;
        }
    }

    public static void multiplicationTable(int n) {
        for (int c = 1; c < n+1; c++) {
            
            System.out.print(c + ": ");
           
            
            for(int b = 1; b <n+1 ; b++) {
                System.out.print(c*b+ " ");
                
      
                }   System.out.println();}
        
            
    }
            
        
      

    
        
        
    

    public static int crossSum(int num) {
        int tverrsum=0;
        while (num> 0){
            int siffer =num%10;
            tverrsum+=siffer;
            num=Math.floorDiv(num,10);
               
        } return tverrsum;
    }

}
