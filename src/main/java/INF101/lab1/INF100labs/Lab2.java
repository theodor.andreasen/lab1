package INF101.lab1.INF100labs;


import javax.lang.model.util.ElementScanner14;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/2/
 */
public class Lab2 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        findLongestWords("Nine", "Four", "Five");
        isLeapYear(3952);
        isEvenPositiveInt(17);

        

    }

    public static void findLongestWords(String word1, String word2, String word3) {
        
       
        int length1= word1.length();
        int length2=word2.length();
        int length3=word3.length();
        //System.out.println(length1);
        //System.out.println(length2);
        //System.out.println(length3);
        int størst =Math.max(length2, (Math.max(length1, length3)));
        if (length1 >= størst) 
            System.out.println(word1);
    
        if (length2 >= størst) 
            System.out.println(word2);
        if (length3 >= størst) 
            System.out.println(word3);

      



    }

    public static boolean isLeapYear(int year) {
      
        
        if (year%400==0) {
             return true;
        }else if (year%100==0) {
            return false;
        }else if (year%4==0) {
            return true;
        }else {
            return false;
        }
        
        
        }
        public static boolean isEvenPositiveInt(int num) {
            if (num < 2) {
                return false;
            }else if (num%2==0){
                return true;
    
            }else {
                return false;
            }
        }
    
    }
    
    
        

        

